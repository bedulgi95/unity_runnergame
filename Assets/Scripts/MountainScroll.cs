﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainScroll : MonoBehaviour {

    public float fScroll_Speed = 0.1f;
    private float fScroll_PosX_Offset = -2.0f;
    private float fScroll_posY_Offset = 0.0f;

    // Update is called once per frame
    void Update()
    {
        fScroll_PosX_Offset += Time.deltaTime * fScroll_Speed;

        Renderer render = this.GetComponent<Renderer>();
        render.material.mainTextureOffset = new Vector2(fScroll_PosX_Offset, fScroll_posY_Offset);

		// Scroll Mountain Continue
        if (fScroll_PosX_Offset > 1.0f)
            fScroll_PosX_Offset = -2.0f;
    }
}
