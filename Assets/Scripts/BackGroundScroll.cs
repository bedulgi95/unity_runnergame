﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundScroll : MonoBehaviour {

    public float fScroll_Speed = 0.025f;
    private float fScroll_PosX_Offset = -2.0f;
    private float fScroll_posY_Offset = 0.0f;
	
	// Update is called once per frame
	void Update () {
        fScroll_PosX_Offset += Time.deltaTime * fScroll_Speed;

        Renderer render = this.GetComponent<Renderer>();
        render.material.mainTextureOffset = new Vector2(fScroll_PosX_Offset, fScroll_posY_Offset);

        if (fScroll_PosX_Offset > 1.0f)
            fScroll_PosX_Offset = -2.0f;
	}
}
